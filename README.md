crosstools
==========

This package includes several configuration files that can be passed to crosstool-ng to create a fully-featured toolchain for ARMv6 and ARMv7 systems (specifically designed for the Raspberry Pi boards).

The following files are included:

- **configv6**: The configuration file for ARMv6 arquitechtures
- **configv7hf**: Configuration file for ARMv7 hard float
- createsymlinks.py: An utility to create symbolic links with no prefix attached (so it can be integrated with distcc).
- **x-tools7h.tar.xz**: The toolchain resulting from the **convigv7hf** file.  