#!/usr/bin/python

import os,sys
from os.path import isfile,islink,isdir,join,abspath
from os import listdir
import re

if len(sys.argv) > 1:
	dir = abspath(sys.argv[1])
	try:
		os.stat(dir)
	except OSError:
		print("The directory %s does not exist" % dir)
else:
	dir = os.getcwd()

files = [f for f in listdir(dir) if isfile(join(dir, f)) and not islink(join(dir,f))]
if len(sys.argv) == 3:
	SUFFIX = sys.argv[2]
else:
	SUFFIX = 'arm-unknown-linux-gnueabihf-'

for file in files:
	if SUFFIX in file:
		sym = re.sub(SUFFIX, '', file)
		try:
			os.symlink(join(dir,file),join(dir,sym))
		except OSError:
			os.unlink(sym)
			os.symlink(join(dir,file),join(dir,sym))

	print("Created symlink %s for %s" % (sym,file))

